package de.loemre.interpretee.database;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;

public class TextRepository {

    private TextDao textDao;
    private LiveData<List<Text>> liveText;


    public TextRepository(Application application) {
        TextRoomDatabase db = TextRoomDatabase.getDatabase(application);
        textDao = db.textDao();
        liveText = textDao.getText();
    }

    public LiveData<List<Text>> getText() {
        return liveText;
    }

    public void insert(Text text) {
        TextRoomDatabase.databaseWriteExecutor.execute(() -> {
            textDao.insert(text);
        });
    }

}
