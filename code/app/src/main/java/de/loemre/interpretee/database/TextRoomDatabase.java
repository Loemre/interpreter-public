package de.loemre.interpretee.database;

import android.content.Context;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;
import de.loemre.interpretee.models.WordCloudDataModel;

@Database(entities = {Text.class}, version = 2, exportSchema = true)
@TypeConverters({Converters.class})
public abstract class TextRoomDatabase extends RoomDatabase {

    public abstract TextDao textDao();

    private static volatile TextRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static TextRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (TextRoomDatabase.class) {
                if(INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            TextRoomDatabase.class, "text_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }

        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            // dummy implementation of a text to test everything
            databaseWriteExecutor.execute(() -> {
                // FIXME
                String wordCloud = "just testing right now";


                TextDao dao = INSTANCE.textDao();

                Text text = new Text("At the scale of the Mont Blanc massif, the border between " +
                        "Italy and France passes along most of the main Alpine watershed, from the " +
                        "Aiguille des Glaciers to Mont Dolent. However, its precise location near " +
                        "the summits of Mont Blanc and nearby Dôme du Goûter has been disputed since " +
                        "the 18th century. Italian officials claim the border follows the watershed, " +
                        "splitting both summits between Italy and France, while French officials " +
                        "claim the border avoids the two summits, making both of them in France only. " +
                        "The size of these two (distinct) disputed areas is approximately 65 ha " +
                        "on Mont Blanc and 10 ha on Dôme du Goûter. Since the French Revolution, " +
                        "the issue of the ownership of the summit has been debated. From 1416 to 1792, " +
                        "the entire mountain was within the Duchy of Savoy. In 1723, the Duke of Savoy, " +
                        "Victor Amadeus II, acquired the Kingdom of Sardinia. The resulting state" +
                        " of Sardinia was to become preeminent in the Italian unification.  " +
                        "In September 1792, the French revolutionary Army of the Alps under " +
                        "Anne-Pierre de Montesquiou-Fézensac seized Savoy without much resistance " +
                        "and created a department of the Mont-Blanc. In a treaty of 15 May 1796, " +
                        "Victor Amadeus III of Sardinia was forced to cede Savoy and Nice to France." +
                        " In article 4 of this treaty it says:  The border between the Sardinian " +
                        "kingdom and the departments of the French Republic will be established on " +
                        "a line determined by the most advanced points on the Piedmont side, " +
                        "of the summits, peaks of mountains and other locations subsequently " +
                        "mentioned, as well as the intermediary peaks, knowing: starting from the " +
                        "point where the borders of Faucigny, the Duchy of Aoust and the Valais, " +
                        "to the extremity of the glaciers or Monts-Maudits: first the peaks or " +
                        "plateaus of the Alps, to the rising edge of the Col-Mayor . This act " +
                        "further states that the border should be visible from the town of Chamonix " +
                        "and Courmayeur.  However, neither is the peak of the Mont Blanc visible " +
                        "from Courmayeur nor is the peak of the Mont Blanc de Courmayeur visible " +
                        "from Chamonix because part of the mountains lower down obscure them. " +
                        "After the Napoleonic Wars, the Congress of Vienna restored the King of " +
                        "Sardinia in Savoy, Nice and Piedmont, his traditional territories, " +
                        "overruling the 1796 Treaty of Paris. Forty-five years later, after the " +
                        "Second Italian War of Independence, it was replaced by a new legal act. " +
                        "This act was signed in Turin on 24 March 1860 by Napoleon III and Victor " +
                        "Emmanuel II of Savoy, and deals with the annexation of Savoy (following " +
                        "the French neutrality for the plebiscites held in Tuscany, Modena, Parma " +
                        "and Romagna to join the Kingdom of Sardinia, against the Pope s will). " +
                        "A demarcation agreement, signed on 7 March 1861, defined the new border. " +
                        "With the formation of Italy, for the first time Mont Blanc was located on " +
                        "the border of France and Italy, along the old border on the watershed " +
                        "between the department of Savoy and that of Piedmont formerly belonging " +
                        "to the Kingdom of Savoy. The 1860 act and attached maps are still legally " +
                        "valid for both the French and Italian governments. In the second half of " +
                        "the nineteenth century, on surveys carried out by a cartographer of the " +
                        "French army, Captain JJ Mieulet, a topographic map was published in " +
                        "France, which incorporated the summit into French territory, making the " +
                        "state border deviate from the watershed line, and giving rise to the " +
                        "differences with the maps published in Italy in the same period. " +
                        "Watershed analysis of modern topographic mapping not only places the " +
                        "main peak on the border, but also suggests that the border should " +
                        "follow a line north from the main peak towards Mont Maudit, leaving " +
                        "the south-east ridge at Mont Blanc de Courmayeur entirely in Italy. " +
                        "In 2002 the French and Italian Alpine Clubs published a shared topographic " +
                        "map as part of their  Alps Without Borders  project, attempted to compare " +
                        "the old maps, but the results still lacked clarity. Since 2017 Google " +
                        "Earth has used the maps of the Italian Istituto Geografico Militare and " +
                        "NATO. The latter takes the data from the Italian data of the I.G.M., " +
                        "based upon past treaties in force. The territory that goes from the " +
                        "Torino Hut to the highest peak of the Mont Blanc massif is under the " +
                        "control of the Italian authorities.", wordCloud,
                        "This act further states that the border should be visible from the town of Chamonix and Courmayeur. A demarcation agreement, signed on 7 March 1861, defined the new border. However, its precise location near the summits of Mont Blanc and nearby Dôme du Goûter has been disputed since the 18th century. The size of these two (distinct) disputed areas is approximately 65 ha on Mont Blanc and 10 ha on Dôme du Goûter. The 1860 act and attached maps are still legally valid for both the French and Italian governments.");

                dao.insert(text);

            });
        }
    };

}
