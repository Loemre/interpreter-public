package de.loemre.interpretee.database;

import org.json.JSONArray;

import java.util.ArrayList;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import de.loemre.interpretee.models.WordCloudDataModel;

@Entity(tableName = "processed_text")
public class Text {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "text")
    private String text;
    @ColumnInfo(name = "wordCloud")
    private String wordCloud;
    @ColumnInfo(name = "textAnalysis")
    private String textAnalysis;

    public Text(String text, String wordCloud, String textAnalysis) {
        this.id = 0;
        this.text = text;
        this.wordCloud = wordCloud;
        this.textAnalysis = textAnalysis;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getWordCloud() {
        return wordCloud;
    }

    public String getTextAnalysis() {
        return textAnalysis;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setWordCloud(String wordCloud) {
        this.wordCloud = wordCloud;
    }

    public void setTextAnalysis(String textAnalysis) {
        this.textAnalysis = textAnalysis;
    }
}
