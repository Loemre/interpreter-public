package de.loemre.interpretee;

import android.accessibilityservice.AccessibilityService;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import android.widget.FrameLayout;

import java.util.ArrayList;

import androidx.lifecycle.LifecycleService;
import de.loemre.interpretee.activities.MainActivity;

public class TextReaderAccessibilityService extends AccessibilityService {

    private static final String TAG = TextReaderAccessibilityService.class.getName();
    private static ArrayList<String> retrievedText;
    private static boolean recording = false;

    ArrayList<AccessibilityNodeInfo> nodeInfoArrayList = new ArrayList<AccessibilityNodeInfo>();
    AccessibilityNodeInfo rootNode;

    FrameLayout frameLayout;

    @Override
    protected void onServiceConnected() {

        Log.i(TAG, "Service Connected");

        // create an overlay
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        frameLayout = new FrameLayout(this);

        WindowManager.LayoutParams windowManagerLayoutParams = new WindowManager.LayoutParams();
        windowManagerLayoutParams.type = WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY;
        windowManagerLayoutParams.format = PixelFormat.TRANSLUCENT;
        windowManagerLayoutParams.flags |= WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        windowManagerLayoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        windowManagerLayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        windowManagerLayoutParams.gravity = Gravity.TOP;

        LayoutInflater inflater = LayoutInflater.from(this);

        inflater.inflate(R.layout.accessibility_service_record_button, frameLayout);
        windowManager.addView(frameLayout, windowManagerLayoutParams);

        configureRecordButton();
        configureWordCloudButton();

        retrievedText = new ArrayList<String>();

    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {

        if(recording) {

            int eventType = event.getEventType();

            switch (eventType) {

                case AccessibilityEvent.TYPE_VIEW_TEXT_SELECTION_CHANGED:

                    // int selectionStart = event.getSource().getTextSelectionStart();
                    // int selectionEnd = event.getSource().getTextSelectionEnd();

                    String textInSelection = event.getText().toString();
                    retrievedText.add(textInSelection);
                    break;

                    // if(selectionStart == selectionEnd) return;

                    // String text = event.getText().toString().substring(selectionStart, selectionEnd + 1);

                    // Log.d(TAG, "onAccessibilityEvent, selectedText " + text);
                // FIXME: No child views found or something, cannot retrieve text
                case AccessibilityEvent.TYPE_VIEW_SCROLLED:
                    rootNode = getRootInActiveWindow();

                    findChildViews(rootNode);
                    for(AccessibilityNodeInfo mNode : nodeInfoArrayList) {

                        if(mNode.getContentDescription() == null) {
                            continue;
                        }

                        String viewText = mNode.getText().toString();
                        Log.d(TAG, "onAccessibilityEvent: " + viewText);
                        retrievedText.add(viewText);

                    }

                    break;

            }
        }

    }

    @Override
    public void onInterrupt() {
    }

    private void findChildViews(AccessibilityNodeInfo parentView) {

            try {

                int childCount = parentView.getChildCount();
                Log.d(TAG, "findChildViews: " + childCount);

                if (parentView == null || parentView.getClassName() == null) {
                    return;
                }

                if (childCount == 0 && (parentView.getClassName().toString()
                        .contentEquals("android.view.TextView"))) {
                    nodeInfoArrayList.add(parentView);
                } else {
                    for (int i = 0; i < childCount; i++) {
                        findChildViews(parentView.getChild(i));
                    }
                }

            } catch (Exception e) {

                Log.e(TAG, "Exception in Text Reader Service: " + e.toString());

            }

    }


    private void configureRecordButton() {



        Button btn_accessibilityServiceRecordButton =
                (Button) frameLayout.findViewById(R.id.btn_accessibilityResetStringsButton);

        btn_accessibilityServiceRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "record button clicked");

                // Clear the previous text for new recording
                if(!recording) retrievedText.clear();
                else {
                    PersistableBundle bundle = new PersistableBundle();
                    bundle.putString("sendText", retrievedText.toString());
                    ComponentName componentName = new ComponentName(
                            TextReaderAccessibilityService.this, APIJobService.class);
                    JobInfo info = new JobInfo.Builder(123, componentName)
                            .setRequiresCharging(false)
                            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                            .setExtras(bundle)
                            .build();

                    JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
                    int resultCode = scheduler.schedule(info);
                    if(resultCode == JobScheduler.RESULT_SUCCESS) {
                        Log.d(TAG, "onClick: Job scheduled");
                    } else {
                        Log.e(TAG, "onClick: Job scheduling failed");
                    }
                }
                recording = !recording;

            }
        });

    }

    private void configureWordCloudButton() {

        Button btn_accessibilityServiceGetWordCloud =
                (Button) frameLayout.findViewById(R.id.btn_accessibilityGetWordCloud);

        btn_accessibilityServiceGetWordCloud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent app = new Intent(Intent.ACTION_VIEW, Uri.parse("http://loemre.de/app"));
                startActivity(app);
            }
        });

    }

    // Getter for text selected

    public static ArrayList<String> getRetrievedText() {
        return retrievedText;
    }

}

