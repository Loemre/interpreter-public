package de.loemre.interpretee.database;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Ignore;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface TextDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Text text);

    @Update(entity = Text.class, onConflict = OnConflictStrategy.REPLACE)
    void update(Text text);

    @Query("DELETE FROM processed_text")
    void delete_all();

    @Query("SELECT * FROM processed_text")
    LiveData<List<Text>> getText();

}
