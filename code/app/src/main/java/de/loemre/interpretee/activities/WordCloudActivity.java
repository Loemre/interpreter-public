package de.loemre.interpretee.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import de.loemre.interpretee.R;
import de.loemre.interpretee.database.TextRepository;
import de.loemre.interpretee.models.WordCloudDataModel;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class WordCloudActivity extends AppCompatActivity {

    WebView wv_wordCloudActivity;
    BottomNavigationView bottomNavigationView;

    public static final String TAG = WordCloudActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_cloud);

        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        TextRepository textRepository = new TextRepository(WordCloudActivity.this.getApplication());

        // get the intent that started the activity
        Intent intent = getIntent();
        ArrayList<WordCloudDataModel> wordSet = new ArrayList<WordCloudDataModel>();

        wordSet = (ArrayList<WordCloudDataModel>) intent.getSerializableExtra("WordSet");

        // fill the word cloud web view
        wv_wordCloudActivity = findViewById(R.id.wv_wordCloudActivity);

        WebSettings webSettings = wv_wordCloudActivity.getSettings();
        webSettings.setJavaScriptEnabled(true);

        wv_wordCloudActivity.loadUrl("file:///android_asset/d3.html");

        // final word set to use in inner method
        ArrayList<WordCloudDataModel> finalWordSet = wordSet;
        wv_wordCloudActivity.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("wordCloud([");
                for(int i = 0; i < finalWordSet.size(); i++) {
                    stringBuffer.append("{ word: '")
                            .append(finalWordSet.get(i).getWord()).append("', ")
                            .append("size: '").append(finalWordSet.get(i).getFrequency())
                            .append("'}");
                    if(i < finalWordSet.size()-1) {
                        stringBuffer.append(",");
                    }
                }
                stringBuffer.append("])");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    wv_wordCloudActivity.evaluateJavascript(stringBuffer.toString(), null);
                } else {
                    wv_wordCloudActivity.loadUrl("javascript:" + stringBuffer.toString());
                }
            }
        });


        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

    }

    private final BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    switch (item.getItemId()) {
                        case R.id.mI_menuHome:
                            Intent mainActivityIntent =
                                    new Intent(WordCloudActivity.this, MainActivity.class);
                            startActivity(mainActivityIntent);
                            break;
                        case R.id.mI_wordCloud:
                            break;
                        case R.id.mI_textAnalysis:
                            Intent textAnalysisIntent =
                                    new Intent(WordCloudActivity.this, TextAnalysisActivity.class);
                            startActivity(textAnalysisIntent);
                            break;

                    }

                    return false;
                }
            };
}