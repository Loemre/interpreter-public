package de.loemre.interpretee;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

public class AnalysisDataService {

    public static final String QUERY_FOR_ANALYSIS_SERVICE = "https://interpretee.herokuapp.com/";
    public static final String TAG = AnalysisDataService.class.getName();

    Context context;

    /**
     * Creates an Class to communicate with the API
     * Handles all requests to the cloud service, is used by the {@link APIJobService} class.
     * @param context The current context that is needed to run the Singleton
     * **/

    public AnalysisDataService(Context context) {
        this.context = context;
    }

    public interface WordcloudAnalysisListener {
        void onError(String message);
        void onResponse(JSONObject wordCloud);
    }

    public void postWordCloud(String longText, int wordsNumber, WordcloudAnalysisListener wordcloudAnalysisListener) {

        String url = QUERY_FOR_ANALYSIS_SERVICE + "wordCloud";

        JSONObject postBody = new JSONObject();

        try {
            postBody.put("text", longText);
            postBody.put("words_number", wordsNumber);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, postBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            wordcloudAnalysisListener.onResponse(response);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                wordcloudAnalysisListener.onError(error.toString());
            }
        });

        InterpreteeSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

    }

    public void postTextAnalysis(String longText, WordcloudAnalysisListener wordcloudAnalysisListener) {

        String url = QUERY_FOR_ANALYSIS_SERVICE + "textAnalysis";

        JSONObject postBody = new JSONObject();

        try {
            postBody.put("text", longText);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, postBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            wordcloudAnalysisListener.onResponse(response);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                wordcloudAnalysisListener.onError(error.toString());
            }
        });

        InterpreteeSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

    }

    public void postAll(String longText, int wordsNumber, WordcloudAnalysisListener wordcloudAnalysisListener) {

        String url = QUERY_FOR_ANALYSIS_SERVICE + "all";

        JSONObject postBody = new JSONObject();

        try {
            postBody.put("text", longText);
            postBody.put("words_number", wordsNumber);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, postBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            wordcloudAnalysisListener.onResponse(response);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                wordcloudAnalysisListener.onError(error.toString());
            }
        });

        InterpreteeSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

    }

    public interface ServiceAnalysisListener {
        void onError(String message);
        void onResponse(String message);
    }

    public void testService(ServiceAnalysisListener serviceAnalysisListener) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, QUERY_FOR_ANALYSIS_SERVICE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    serviceAnalysisListener.onResponse(response);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                serviceAnalysisListener.onError("Error in TestService: " + error.toString());
            }
        });

        InterpreteeSingleton.getInstance(context).addToRequestQueue(stringRequest);

    }

}