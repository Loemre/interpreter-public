package de.loemre.interpretee.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import de.loemre.interpretee.APIJobService;
import de.loemre.interpretee.AnalysisDataService;
import de.loemre.interpretee.R;
import de.loemre.interpretee.TextReaderAccessibilityService;
import de.loemre.interpretee.models.WordCloudDataModel;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getName();

    Button btn_getWordCloud, btn_getTextAnalysis, btn_recordText;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_getWordCloud = findViewById(R.id.btn_getWordCloud);
        btn_getTextAnalysis = findViewById(R.id.btn_getTextAnalysis);
        btn_recordText = findViewById(R.id.btn_recordText);


        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        // create click listeners for the buttons


        btn_getWordCloud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ComponentName componentName = new ComponentName(MainActivity.this, APIJobService.class);
                JobInfo info = new JobInfo.Builder(123, componentName)
                        .setRequiresCharging(false)
                        .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                        .build();

                JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
                int resultCode = scheduler.schedule(info);
                if(resultCode == JobScheduler.RESULT_SUCCESS) {
                    Log.d(TAG, "onClick: Job scheduled");
                } else {
                    Log.d(TAG, "onClick: Job scheduling failed");
                }

            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);


    }

    private final BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    switch (item.getItemId()) {
                        case R.id.mI_menuHome:
                            break;
                        case R.id.mI_wordCloud:
                            Intent wordCloudIntent =
                                    new Intent(MainActivity.this, WordCloudActivity.class);
                            startActivity(wordCloudIntent);
                            break;
                        case R.id.mI_textAnalysis:
                            Intent textAnalysisIntent =
                                    new Intent(MainActivity.this, TextAnalysisActivity.class);
                            startActivity(textAnalysisIntent);
                            break;

                    }

                    return false;
                }
            };

}