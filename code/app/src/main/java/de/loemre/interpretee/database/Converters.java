package de.loemre.interpretee.database;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import androidx.room.TypeConverter;
import de.loemre.interpretee.models.WordCloudDataModel;

public class Converters {
    @TypeConverter
    public static ArrayList<WordCloudDataModel> fromString(String value) {
        Type listType = new TypeToken<ArrayList<WordCloudDataModel>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromArrayList(ArrayList<WordCloudDataModel> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }
}