package de.loemre.interpretee.models;

import android.app.Application;

import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import de.loemre.interpretee.database.Text;
import de.loemre.interpretee.database.TextRepository;

public class TextRoomViewModel extends AndroidViewModel {

    private TextRepository mTextRepository;

    private final LiveData<List<Text>> mLiveText;

    public TextRoomViewModel(Application application) {
        super(application);
        mTextRepository = new TextRepository(application);
        mLiveText = mTextRepository.getText();
    }

    public LiveData<List<Text>> getLiveText() {
        return mLiveText;
    }

    public void insert(Text text) {
        mTextRepository.insert(text);
    }

}
