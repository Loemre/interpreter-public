package de.loemre.interpretee.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import de.loemre.interpretee.R;
import de.loemre.interpretee.database.TextRepository;
import de.loemre.interpretee.models.TextRoomViewModel;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import static com.google.gson.reflect.TypeToken.get;

public class TextAnalysisActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;

    private TextRoomViewModel mTextRoomViewModel;
    private String textAnalysis;
    public static final String TAG = TextAnalysisActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_analysis);

        TextView summary = findViewById(R.id.tv_textAnalysisSummary);

        mTextRoomViewModel = new ViewModelProvider(this,
                new ViewModelProvider.AndroidViewModelFactory(getApplication()))
                .get(TextRoomViewModel.class);

        mTextRoomViewModel.getLiveText().observe(this, texts -> {
            textAnalysis = texts.get(0).getTextAnalysis();
            summary.setText(textAnalysis);
            Log.d(TAG, textAnalysis);
        });

        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);







        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

    }

    private final BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    switch (item.getItemId()) {
                        case R.id.mI_menuHome:
                            Intent mainActivityIntent =
                                    new Intent(TextAnalysisActivity.this, MainActivity.class);
                            startActivity(mainActivityIntent);
                            break;
                        case R.id.mI_wordCloud:
                            Intent wordCloudIntent =
                                    new Intent(TextAnalysisActivity.this, WordCloudActivity.class);
                            startActivity(wordCloudIntent);
                            break;
                        case R.id.mI_textAnalysis:
                            break;

                    }

                    return false;
                }
            };
}