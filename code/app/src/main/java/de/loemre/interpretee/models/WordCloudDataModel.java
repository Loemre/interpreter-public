package de.loemre.interpretee.models;

import java.io.Serializable;

import androidx.annotation.NonNull;

public class WordCloudDataModel implements Serializable {

    private String frequency;
    private String word;

    public WordCloudDataModel(String frequency, String word) {
        this.frequency = frequency;
        this.word = word;
    }

    @NonNull
    @Override
    public String toString() {
        return "Frequency: " + frequency + " Word: " + word;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
