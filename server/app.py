# Server Applikation in Python

from flask import Flask, request, jsonify

import nltk as nltk

from methods.word_cloud import WordCloud, sort_cloud_list
from methods.text_processing import detect_language_custom
from methods.analysis import analysis

app = Flask(__name__)

langer_test = "Bisher sind Python Kurs noch nicht direkt auf Methoden zu sprechen gekommen. " \
              "Aber was sind diese Methoden, die es in jeder Programmiersprache gibt? " \
              "Sucht man nach Synonymen von dem Wort „Methode“ stößt man auf „Vorgehen, " \
              "Arbeitsweise und Handhabung“. Wir können also auf ein bestimmtes Objekt " \
              "(in unserem Fall sind die Objekte der Begierde die Listen) vorgegebene Methoden (sprich Möglichkeiten) " \
              "anwenden. Wer sich schon ein bisschen mit Objektorientierter Programmierung auseinandergesetzt hat, " \
              "kennt das. Wir haben ein Objekt und das Objekt hat einerseits Inhalt (sprich Attribute) und mögliche " \
              "vordefinierte Funktionen (sprich Methoden). Und hier kommt auch die Schreibweise zum Vorschein. " \
              "Erst wir das Objekt genannt und dann wird die Methode an dieses Objekt mit einem Punkt „verkettet“. " \
              "Gegebenenfalls kann man noch weitere Parameter für die Funktionsweise der Methode mit übergeben. " \
              "Klarer wird es im konkreten Beispiel (natürlich hier mit Listen). " \
              "Nutzen wir die Liste mit den Vornamen und lassen uns den zweiten Namen (sprich Indexnummer 1) ausgeben:"


@app.route('/')
def communicating():
    return 'Connection established'


@app.route('/wordCloud', methods=['GET', 'POST'])
def word_cloud():
    if request.method == 'GET':
        cloud = WordCloud(langer_test)
        word_cloud_list = cloud.word_cloud()
        sorted_dict = sort_cloud_list(word_cloud_list, True, 5)
        message = {
            'status': 200,
            'message': 'OK',
            'word': sorted_dict
        }
        resp = jsonify(message)
        resp.status_code = 200
        return resp
    elif request.method == 'POST':
        body = request.get_json(force=True)
        cloud = WordCloud(body["text"])
        top_words = body["words_number"]
        word_cloud_list = cloud.word_cloud()
        sorted_dict = sort_cloud_list(word_cloud_list, top_words)
        lang = detect_language_custom(body["text"])
        message = {
            'status': 200,
            'message': 'OK',
            'res': 'POST to world cloud',
            'lang': lang,
            'words': sorted_dict
        }
        resp = jsonify(message)
        resp.status_code = 200
        return resp


@app.route('/textAnalysis', methods=['POST'])
def text_analysis():
    if request.method == 'POST':
        body = request.get_json(force=True)
        lang = detect_language_custom(body["text"])

        if len(nltk.sent_tokenize(body["text"])) < 5:
            message = {
                'status': 400,
                'message': 'Bad Request',
                'res': 'post to text analysis',
                'err': 'text too short'
            }
            resp = jsonify(message)
            resp.status_code = 400
            return resp

        summary = analysis(body["text"])
        message = {
            'status': 200,
            'message': 'OK',
            'res': 'POST to text analysis',
            'lang': lang, 
            'summ': summary
        }
        resp = jsonify(message)
        resp.status_code = 200
        return resp

@app.route('/all', methods=['POST'])
def post_all():
    if request.method == 'POST':
        body = request.get_json(force=True)
        lang = detect_language_custom(body["text"])

        if len(nltk.sent_tokenize(body["text"])) < 5:
            message = {
                'status': 400,
                'message': 'Bad Request',
                'res': 'post to text analysis',
                'err': 'text too short, use word cloud route'
            }
            resp = jsonify(message)
            resp.status_code = 400
            return resp

        summary = analysis(body["text"])
        cloud = WordCloud(body["text"])
        top_words = body["words_number"]
        word_cloud_list = cloud.word_cloud()
        sorted_dict = sort_cloud_list(word_cloud_list, top_words)

        message = {
            'status': 200,
            'message': 'OK',
            'res': 'POST to all',
            'lang': lang,
            'words': sorted_dict,
            'summ': summary
        }
        resp = jsonify(message)
        resp.status_code = 200
        return resp


if __name__ == '__main__':
    """ in production use the following lines and disable debug mode in PATH
    from waitress import serve
    serve(app, host="0.0.0.0", port=8080) """

    app.run()
