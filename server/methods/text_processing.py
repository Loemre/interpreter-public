# all text processing magic will happen here

import re
import nltk as nltk
from langdetect import detect


def count_words(input_text):
    # remove extra space
    input_text = re.sub(r'\s+', ' ', input_text)

    formatted_input_text = re.sub('[^a-zA-Z]', ' ', input_text)
    formatted_input_text = re.sub(r'\s+', ' ', formatted_input_text)

    lang = detect_language_custom(input_text)
    stopwords = nltk.corpus.stopwords.words(lang)

    word_frequencies = {}
    for word in nltk.word_tokenize(formatted_input_text):
        if word not in stopwords:
            if word not in word_frequencies.keys():
                word_frequencies[word] = 1
            else:
                word_frequencies[word] += 1

    return word_frequencies


def detect_language_custom(input_text):
    switcher = {
        'de': 'german',
        'en': 'english'
    }
    # fallback option is the english language
    lang = switcher.get(detect(input_text), 'english')

    return lang


class TextProcessing:
    pass
