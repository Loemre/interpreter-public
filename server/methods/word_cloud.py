# all word cloud operations will be executed here

import heapq
from methods.text_processing import count_words, detect_language_custom


def sort_cloud_list(word_dict, number=10):
    sorted_word_dict = {}
    words_all = 0

    maximum_frequency = max(word_dict.values())

    for word in word_dict:
        # to get frequencies add all words found (without stopwords)
        words_all = words_all + word_dict[word]
    sorted_word_list = heapq.nlargest(number, word_dict, key=word_dict.get)

    for val in sorted_word_list:
        sorted_word_dict[val] = word_dict.get(val)/maximum_frequency

    return sorted_word_dict


class WordCloud:
    """Soll eine Word Cloud erstellen, mehr Hilfe ist TODO"""

    def __init__(self, input_text):
        self.input_text = input_text

    def word_cloud(self):
        tmp = count_words(self.input_text)
        sorted_words = heapq.nlargest(12, tmp, key=tmp.get)
        max_words = {}
        for word in sorted_words:
            max_words[word] = tmp.get(word)
        # return max_words
        return tmp
